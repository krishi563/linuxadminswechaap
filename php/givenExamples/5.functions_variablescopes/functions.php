<?php

	//function with no arguments
	echo "<br>function with no arguments<br>";
	function hello(){
		echo "Hello World <br>";
	}
	hello();
	echo "<br>function with arguments<br>";
	//function with arguments
	function helloUser($user){
		echo "Hello ".$user;
	}
	helloUser("saikiran");
	
?>